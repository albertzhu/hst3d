#!/usr/bin/python
from astropy.io import ascii
import matplotlib.pyplot as plt

class Spectrum(object):

	def __init__(self, inputpath = None, namesource = None):
		if namesource is None:
			print("A spectrum file must "
				"be specified to create a spectrum.")
		self.namesource = namesource
		self.inputpath = inputpath
		self.namefile = 'cosmos-01-G141_'+ str(self.namesource) + '.1D.ascii'
		self.datafile = ascii.read(self.inputpath + self.namefile)
		self._wave = None
		self._flux = None
	@property
	def wave(self):
		''' Returns the RA of this spectrum in degrees. '''
		if self._wave == None:
			self._wave = self.datafile['wave']
		return self._wave
	@property
	def flux(self):
		''' Returns the RA of this spectrum in degrees. '''
		if self._flux == None:
			self._flux = self.datafile['flux']
		return self._flux

	def plot_spectrum(self, out_dir):
		plt.plot(self.wave, self.flux, color = 'b')
		plt.xlabel(r'Wavelength ($\AA$)')
		plt.ylabel(r'Flux')

		plt.savefig(out_dir + self.namesource + '.pdf')
