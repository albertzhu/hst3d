#!/usr/bin/python

from spectrum import Spectrum
import matplotlib.pyplot as plt


filepath = '../cosmos-01/1D/ASCII/'
outpath = 'files/'
id_source = '21218'


spec = Spectrum(filepath, id_source)
spec.plot_spectrum(outpath)
#plt.show()
